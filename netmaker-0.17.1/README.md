
### Express Installation
- login as root
- run this script
```bash
{
    mkdir -p /root/nmcfg
    cd /root/nmcfg
    wget -qO ./nm-quick-interactive.sh https://gitlab.com/opnx/script/-/raw/main/netmaker-0.17.1/nm-quick-interactive.sh
    chmod +x ./nm-quick-interactive.sh
    ./nm-quick-interactive.sh
}
```